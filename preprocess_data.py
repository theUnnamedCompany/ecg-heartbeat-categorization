"""
Tools to preprocess data from ECG Dataset
-----------------------------------------
@author: Jose Carlos Garcia.
@email: jcarlos3094@gmail.com
"""

from sklearn.utils import shuffle
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import OneHotEncoder
import numpy as np
from scipy.signal import resample
import random
np.random.seed(42)

###############################
# Data augmentation algorithm #
###############################
def stretch(x):
    """ 
    Function of strech the signal using Fourier transform via scipy library.
    """
    l = int(187.0 * (1 + (random.random()-0.5)/3.0))
    y = resample(x, l)
    if l < 187:
        y_ = np.zeros(shape=(187, ))
        y_[:l] = y
    else:
        y_ = y[:187]
    return y_

def amplify(x):
    """
    Function of amplification of the signal
    """
    alpha = (random.random()-0.5)
    factor = -alpha*x + (1+alpha)
    return x*factor

def augment(x, augment_size):
    result = np.zeros(shape = (augment_size, 187))
    for i in range(augment_size-1):
        if random.random() < 0.33:
            new_y = stretch(x)
        elif random.random() < 0.66:
            new_y = amplify(x)
        else:
            new_y = stretch(x)
            new_y = amplify(new_y)
        result[i, :] = new_y
    return result

def data_augmentation(X, y, arr, classe, augment_size):
    """
    Function of augmentation of data to equiparate classes
    """
    if classe=='N':
        x_augmented = np.apply_along_axis(augment, axis=1, arr=arr, augment_size=augment_size).reshape(-1, 187)
        y_augmented = np.ones(shape=(x_augmented.shape[0],), dtype=int)*0
    
        X = np.vstack([X, x_augmented])
        y = np.hstack([y, y_augmented])


    elif classe=='S':
        x_augmented = np.apply_along_axis(augment, axis=1, arr=arr, augment_size=augment_size).reshape(-1, 187)
        y_augmented = np.ones(shape=(x_augmented.shape[0],), dtype=int)*1

        X = np.vstack([X, x_augmented])
        y = np.hstack([y, y_augmented])

    elif classe=='V':
        x_augmented = np.apply_along_axis(augment, axis=1, arr=arr, augment_size=augment_size).reshape(-1, 187)
        y_augmented = np.ones(shape=(x_augmented.shape[0],), dtype=int)*2

        X = np.vstack([X, x_augmented])
        y = np.hstack([y, y_augmented])

    elif classe=='F':
        x_augmented = np.apply_along_axis(augment, axis=1, arr=arr, augment_size=augment_size).reshape(-1, 187)
        y_augmented = np.ones(shape=(x_augmented.shape[0],), dtype=int)*3

        X = np.vstack([X, x_augmented])
        y = np.hstack([y, y_augmented])

    elif classe=='Q':
        x_augmented = np.apply_along_axis(augment, axis=1, arr=arr, augment_size=augment_size).reshape(-1, 187)
        y_augmented = np.ones(shape=(x_augmented.shape[0],), dtype=int)*4

        X = np.vstack([X, x_augmented])
        y = np.hstack([y, y_augmented])

    elif classe=='C0':
        x_augmented = np.apply_along_axis(augment, axis=1, arr=arr, augment_size=augment_size).reshape(-1, 187)
        y_augmented = np.ones(shape=(x_augmented.shape[0],), dtype=int)*0

        X = np.vstack([X, x_augmented])
        y = np.hstack([y, y_augmented])

    elif classe=='C1':
        x_augmented = np.apply_along_axis(augment, axis=1, arr=arr, augment_size=augment_size).reshape(-1, 187)
        y_augmented = np.ones(shape=(x_augmented.shape[0],), dtype=int)*1

        X = np.vstack([X, x_augmented])
        y = np.hstack([y, y_augmented])

    else:
        print("Introduce a correct classe")
        
    return X, y

##########################################
# Train and test balanced split function #
##########################################

def train_testbalanced_split(X, y, model, classes, test_size, random_state):
    """
    Function to split the dataset into training, and testing dataset balanced
    --------------------------------------------------------------------------
    classes[model==0] = [N, S, V, F, Q]
    classes[model==1] = [C0, C1]
    """
    if model == 0:
        batch = int(X.shape[0]*np.clip(test_size, a_min=0.0, a_max=1.0))
        N_subTest = np.random.choice(classes[0], batch)
        S_subTest = np.random.choice(classes[1], batch)
        V_subTest = np.random.choice(classes[2], batch)
        F_subTest = np.random.choice(classes[3], batch)
        Q_subTest = np.random.choice(classes[4], batch)

        X_test = np.vstack([X[N_subTest], X[S_subTest], X[V_subTest], X[F_subTest], X[Q_subTest]])
        y_test = np.hstack([y[N_subTest], y[S_subTest], y[V_subTest], y[F_subTest], y[Q_subTest]])

        X_train = np.delete(X, [N_subTest, S_subTest, V_subTest, F_subTest, Q_subTest], axis=0)
        y_train = np.delete(y, [N_subTest, S_subTest, V_subTest, F_subTest, Q_subTest], axis=0)

    if model == 1:
        batch = int(X.shape[0]*np.clip(test_size, a_min=0.0, a_max=1.0))
        C0_subTest = np.random.choice(classes[0], batch)
        C1_subTest = np.random.choice(classes[1], batch)

        X_test = np.vstack([X[C0_subTest], X[C1_subTest]])
        y_test = np.hstack([y[C0_subTest], y[C1_subTest]])

        X_train = np.delete(X, [C0_subTest, C1_subTest], axis=0)
        y_train = np.delete(y, [C0_subTest, C1_subTest], axis=0)

    X_train, y_train = shuffle(X_train, y_train, random_state=random_state)
    X_test, y_test = shuffle(X_test, y_test, random_state=random_state)
    
    return X_train, X_test, y_train, y_test


#############################
# Keras preprocess function #
#############################

def preprocess_input(X_train, X_valid, X_test, y_train, y_valid, y_test):
    """
    Function to preprocess input for Keras
    --------------------------------------
    - Expand attribute vector
    - One hot encoding classes
    """

    X_train = np.expand_dims(X_train, 2)
    X_test = np.expand_dims(X_test, 2)
    X_valid = np.expand_dims(X_valid, 2)

    ohe = OneHotEncoder()
    y_train = ohe.fit_transform(y_train.reshape(-1,1))
    y_test = ohe.transform(y_test.reshape(-1,1))
    y_valid = ohe.transform(y_valid.reshape(-1,1))
    
    return X_train, X_valid, X_test, y_train, y_valid, y_test
