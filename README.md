The dataset can be download thought the following link:
https://www.kaggle.com/shayanfazeli/heartbeat/downloads/heartbeat.zip/1

Software requirements:
- Jupyter Notebook
- Python >= 3.5.2
- Numpy >= 1.16.2
- Scipy >= 0.17.0
- Pandas >= 0.17.1
- Matplotlib >= 3.0.3
- scikit-learn >= 0.20.3
- imbalance-learn >= 0.4.3
- Keras >= 2.2.4
- TensorFlow >= 1.13.1

Instructions
- The project report is in Capstone_Project.pdf file. In this file, you can check the code and jupyer notebook executed in the Appendixes.
- ECG_Recognition.ipynb is the code ready to be executed.
- preprocess_data.py and ecg_models.py are library developed to use by ECG_Recognition.ipynb