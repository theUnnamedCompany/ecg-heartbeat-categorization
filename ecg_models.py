"""
Models developing for ECG recognition.
--------------------------------------
@author: Jose Carlos Garcia.
@email: jcarlos3094@gmail.com
"""

from keras.models import Model, Sequential
from keras.layers import Input, Dense, Conv1D, MaxPooling1D, Softmax, Add, Flatten, Activation, BatchNormalization
from keras import backend as K


def extract_bottleneck_features(X_train):
    """
    1D CNN Block
    -------------
    Implementation of a CNN model to extract ECG features
    """
    K.clear_session()

    i = Input(shape=X_train.shape[1:])
    C = Conv1D(filters=32, kernel_size=5, strides=1)(i)

    C11 = Conv1D(filters=32, kernel_size=5, strides=1, padding='same')(C)
    A11 = Activation("relu")(C11)
    C12 = Conv1D(filters=32, kernel_size=5, strides=1, padding='same')(A11)
    R11 = Add()([C12, C])
    A12 = Activation("relu")(R11)
    M11 = MaxPooling1D(pool_size=5, strides=2)(A12)


    C21 = Conv1D(filters=32, kernel_size=5, strides=1, padding='same')(M11)
    A21 = Activation("relu")(C21)
    C22 = Conv1D(filters=32, kernel_size=5, strides=1, padding='same')(A21)
    R21 = Add()([C22, M11])
    A22 = Activation("relu")(R21)
    M21 = MaxPooling1D(pool_size=5, strides=2)(A22)


    C31 = Conv1D(filters=32, kernel_size=5, strides=1, padding='same')(M21)
    A31 = Activation("relu")(C31)
    C32 = Conv1D(filters=32, kernel_size=5, strides=1, padding='same')(A31)
    R31 = Add()([C32, M21])
    A32 = Activation("relu")(R31)
    M31 = MaxPooling1D(pool_size=5, strides=2)(A32)


    C41 = Conv1D(filters=32, kernel_size=5, strides=1, padding='same')(M31)
    A41 = Activation("relu")(C41)
    C42 = Conv1D(filters=32, kernel_size=5, strides=1, padding='same')(A41)
    R41 = Add()([C42, M31])
    A42 = Activation("relu")(R41)
    M41 = MaxPooling1D(pool_size=5, strides=2)(A42)


    C51 = Conv1D(filters=32, kernel_size=5, strides=1, padding='same')(M41)
    A51 = Activation("relu")(C51)
    C52 = Conv1D(filters=32, kernel_size=5, strides=1, padding='same')(A51)
    R51 = Add()([C52, M41])
    A52 = Activation("relu")(R51)
    M51 = MaxPooling1D(pool_size=5, strides=2)(A52)

    F1 = Flatten()(M51)
    A6 = Activation("relu")(F1)

    return Model(inputs=i, outputs=A6)

def full_connect_NN(features, number_of_classes):
    """
    Full Connect Neural Network
    """
    model = Sequential()
    model.add(features)
    model.add(BatchNormalization())
    model.add(Dense(32))
    model.add(Dense(number_of_classes))
    model.add(Softmax())
    return model